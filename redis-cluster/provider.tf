terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.6.0"
    }
  }
}

locals {
  # Use available Redis package version from
  # https://launchpad.net/~redislabs/+archive/ubuntu/redis/+index
  redis_version         = "6:6.2.1-4rl1~focal1"
  redis_port            = 9736
  redis_cluster_enabled = "yes"
  do_project_id         = "dee04c2d-d89e-40e8-8712-e5967e2d332d"
}

variable "do_token" {
  type        = string
  description = "Token from Digitalocean"
}

variable "pvt_key" {
  type        = string
  description = "Path to deployer private key for connection to DO (e.g. ~/.ssh/id_rsa)"
}

variable "do_ssh_key_name" {
  type        = string
  description = "Deployer's SSH key name as specified in Digitalocean console"
}

provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "terraform" {
  name = var.do_ssh_key_name
}

data "digitalocean_project" "terraform_testing"{
  id = local.do_project_id
}

# data "template_file" "install_redis" {
#   template = file("scripts/install_redis.sh.tpl")
#   vars = {
#     version = local.redis_version
#   }
# }

# data "template_file" "redis_master_config" {
#   template = file("config/redis.conf.tpl")
#   vars = {
#     port            = local.redis_port
#     cluster_enabled = local.redis_cluster_enabled
#   }
# }
