#!/bin/bash
# install Redis from package manager

export PATH=$PATH:/usr/bin

echo "vm.overcommit_memory = 1" >> /etc/sysctl.conf
sysctl vm.overcommit_memory=1

sudo add-apt-repository ppa:redislabs/redis -y
sudo apt-get update
sudo apt-get -y install net-tools redis=${version}
