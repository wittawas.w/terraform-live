#!/bin/bash
# Enable Redis service in systemd

sudo systemctl enable redis-server
sudo systemctl restart redis.service
