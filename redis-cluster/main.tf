resource "digitalocean_tag" "redis" {
  name = "redis"
}

resource "digitalocean_droplet" "sample_redis_1" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-1"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }
}

resource "digitalocean_droplet" "sample_redis_2" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-2"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }
}

resource "digitalocean_droplet" "sample_redis_3" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-3"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }
}

resource "digitalocean_droplet" "sample_redis_4" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-4"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }
}

resource "digitalocean_droplet" "sample_redis_5" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-5"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }
}

resource "digitalocean_droplet" "sample_redis_6" {
  image = "ubuntu-20-04-x64"
  name = "simple-redis-6"
  region = "sgp1"
  size = "s-1vcpu-1gb"
  private_networking = true
  ssh_keys = [
    data.digitalocean_ssh_key.terraform.id
  ]
  tags = [
    digitalocean_tag.redis.id
  ]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/scripts",
      "mkdir -p /tmp/config"
    ]
  }

  provisioner "file" {
    content = templatefile("scripts/install_redis.sh.tpl", { version = local.redis_version })
    destination = "/tmp/scripts/install_redis.sh"
  }

  provisioner "file" {
    source      = "scripts/enable_redis.sh"
    destination = "/tmp/scripts/enable_redis.sh"
  }

  provisioner "file" {
    content = templatefile("config/redis.conf.tpl", { port = local.redis_port, cluster_enabled = local.redis_cluster_enabled })
    destination = "/tmp/config/redis.conf"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/scripts/install_redis.sh",
      "sudo /tmp/scripts/install_redis.sh",
      "sudo mv /tmp/config/redis.conf /etc/redis/redis.conf",
      "chmod +x /tmp/scripts/enable_redis.sh",
      "sudo /tmp/scripts/enable_redis.sh",
    ]
  }

  # Execute command to create cluster
  # provisioner "remote-exec" {
  #   inline = ["redis-cli --cluster create ${digitalocean_droplet.sample_redis_1.ipv4_address}:${local.redis_port} ${digitalocean_droplet.sample_redis_2.ipv4_address}:${local.redis_port} ${digitalocean_droplet.sample_redis_3.ipv4_address}:${local.redis_port} ${digitalocean_droplet.sample_redis_4.ipv4_address}:${local.redis_port} ${digitalocean_droplet.sample_redis_5.ipv4_address}:${local.redis_port} ${self.ipv4_address}:${local.redis_port} --cluster-replicas 1"]
  # }
}

resource "digitalocean_project_resources" "terraform_testing" {
  project = data.digitalocean_project.terraform_testing.id
  resources = [
    digitalocean_droplet.sample_redis_1.urn,
    digitalocean_droplet.sample_redis_2.urn,
    digitalocean_droplet.sample_redis_3.urn,
    digitalocean_droplet.sample_redis_4.urn,
    digitalocean_droplet.sample_redis_5.urn,
    digitalocean_droplet.sample_redis_6.urn
  ]
}

resource "digitalocean_firewall" "redis" {
  name = "sample-redis-cluster"

  droplet_ids = [
    digitalocean_droplet.sample_redis_1.id,
    digitalocean_droplet.sample_redis_2.id,
    digitalocean_droplet.sample_redis_3.id,
    digitalocean_droplet.sample_redis_4.id,
    digitalocean_droplet.sample_redis_5.id,
    digitalocean_droplet.sample_redis_6.id
  ]

  inbound_rule {
    protocol         = "tcp"
    port_range       = 22
    source_addresses = [ "0.0.0.0/0" ]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = local.redis_port
    source_addresses = [ "0.0.0.0/0" ]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = local.redis_port
    destination_addresses = [ "0.0.0.0/0" ]
  }
}
