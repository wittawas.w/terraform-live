# bind 127.0.0.1 ::1
protected-mode no
port ${port}
tcp-backlog 511
timeout 0
tcp-keepalive 300
daemonize yes
supervised systemd

pidfile /var/run/redis/redis-server.pid
loglevel notice
logfile /var/log/redis/redis-server.log


databases 16
always-show-logo yes

save 900 1
save 300 10
save 60 10000

stop-writes-on-bgsave-error yes
rdbcompression yes
rdbchecksum yes
dbfilename dump.rdb

rdb-del-sync-files no
dir /var/lib/redis

################################# REPLICATION #################################

# replicaof <masterip> <masterport>
# masterauth <master-password>
# masteruser <username>

replica-serve-stale-data yes
replica-read-only yes
repl-diskless-sync no
repl-diskless-sync-delay 5

repl-diskless-load disabled
# repl-ping-replica-period 10
# repl-timeout 60
repl-disable-tcp-nodelay no
# repl-backlog-size 1mb
# repl-backlog-ttl 3600
replica-priority 100

# min-replicas-to-write 3
# min-replicas-max-lag 10
# min-replicas-max-lag is set to 10.

################################## SECURITY ###################################

acllog-max-len 128
# aclfile /etc/redis/users.acl
# requirepass foobared

################################### CLIENTS ####################################

# maxclients 10000

############################## MEMORY MANAGEMENT ################################

maxmemory 256mb
maxmemory-policy allkeys-lru
# maxmemory-policy volatile-ttl
# maxmemory-policy noeviction
# maxmemory-samples 5
# replica-ignore-maxmemory yes
# active-expire-effort 1

############################# LAZY FREEING ####################################

lazyfree-lazy-eviction no
lazyfree-lazy-expire no
lazyfree-lazy-server-del no
replica-lazy-flush no

lazyfree-lazy-user-del no

################################ THREADED I/O #################################

# io-threads 4
# io-threads-do-reads no

############################## APPEND ONLY MODE ###############################

# AOF is off by default. Turn on to prevent data loss.
appendonly no
appendfilename "appendonly.aof"

# Change fsync to "always" to have (almost) no data loss
# but significanly slower than "everysec"

# appendfsync always
appendfsync everysec
# appendfsync no
no-appendfsync-on-rewrite no

auto-aof-rewrite-percentage 100
auto-aof-rewrite-min-size 64mb
aof-load-truncated yes
aof-use-rdb-preamble yes

################################ LUA SCRIPTING  ###############################

lua-time-limit 5000

################################ REDIS CLUSTER  ###############################

cluster-enabled ${cluster_enabled}
# cluster-config-file nodes-6379.conf

cluster-node-timeout 15000
# cluster-replica-validity-factor 10
# cluster-migration-barrier 1
# cluster-require-full-coverage yes
# cluster-replica-no-failover no
# cluster-allow-reads-when-down no

########################## CLUSTER DOCKER/NAT support  ########################

# In certain deployments, Redis Cluster nodes address discovery fails, because
# addresses are NAT-ted or because ports are forwarded (the typical case is
# Docker and other containers).
#
# In order to make Redis Cluster working in such environments, a static
# configuration where each node knows its public address is needed. The
# following two options are used for this scope, and are:
#
# * cluster-announce-ip
# * cluster-announce-port
# * cluster-announce-bus-port
#
# Each instruct the node about its address, client port, and cluster message
# bus port. The information is then published in the header of the bus packets
# so that other nodes will be able to correctly map the address of the node
# publishing the information.
#
# If the above options are not used, the normal Redis Cluster auto-detection
# will be used instead.
#
# Note that when remapped, the bus port may not be at the fixed offset of
# clients port + 10000, so you can specify any port and bus-port depending
# on how they get remapped. If the bus-port is not set, a fixed offset of
# 10000 will be used as usually.
#
# Example:
#
# cluster-announce-ip 10.1.1.5
# cluster-announce-port 6379
# cluster-announce-bus-port 6380

################################## SLOW LOG ###################################

slowlog-log-slower-than 10000
slowlog-max-len 128

################################ LATENCY MONITOR ##############################

latency-monitor-threshold 0

############################# EVENT NOTIFICATION ##############################

notify-keyspace-events ""

############################### ADVANCED CONFIG ###############################

hash-max-ziplist-entries 512
hash-max-ziplist-value 64

list-max-ziplist-size -2
list-compress-depth 0

set-max-intset-entries 512

zset-max-ziplist-entries 128
zset-max-ziplist-value 64

hll-sparse-max-bytes 3000

stream-node-max-bytes 4096
stream-node-max-entries 100
activerehashing yes

client-output-buffer-limit normal 0 0 0
client-output-buffer-limit replica 256mb 64mb 60
client-output-buffer-limit pubsub 32mb 8mb 60

hz 10
dynamic-hz yes
aof-rewrite-incremental-fsync yes
rdb-save-incremental-fsync yes
jemalloc-bg-thread yes
